if ( ! $?site_path ) then
    if ( -f /etc/debian_version || -f /etc/redhat_release ) then
        set site_path=( \
            /usr/local/bin /usr/bin /bin /usr/bin/X11 /usr/sbin /sbin \
            /usr/games /usr/sweet/bin /usr/pubsw/bin /usr/pubsw/X/bin \
            )
    else
        set site_path=( \
            /usr/local/bin /usr/sweet/bin /usr/pubsw/bin \
            /usr/bin /bin /usr/sbin /sbin /usr/proc/bin \
            /usr/local/X/bin /usr/sweet/X/bin /usr/pubsw/X/bin /usr/bin/X11 \
            /opt/SUNWspro/bin /opt/langtools/bin /usr/lang /usr/ccs/bin \
            /usr/ucb /usr/bsd /usr/etc \
            /usr/dt/bin /usr/openwin/bin /usr/demos/bin /usr/audio/bin \
            /usr/games \
            )
    endif
endif

set user_path=( ~/bin $site_path ~ /usr/kerberos/bin ~/jruby/bin )
set path=( $site_path )

alias upath "set path=( $user_path )"
alias spath "set path=( $site_path )"

#------------#
# All Shells #
#------------#

umask 022               # file protection no read or write for others
                        # umask 022 is no write but read for others

limit coredumpsize 0    # don't write core dumps
set noclobber           # don't overwrite existing file w/ redirect
set rmstar              # prompt before executing rm *
alias cp 'cp -i'        # prompt before overwriting file
alias mv 'mv -i'        # prompt before overwriting file
alias rm 'rm -i'        # prompt before removing file

# Set the scratch directory location
set s = /srv/scratch/akkornel

# Explicitly tell Git to use the local SSH (needed for Eclipse)
setenv GIT_SSH /usr/bin/ssh

# Identification information for packaging
setenv DEBEMAIL 'akkornel@stanford.edu'
setenv DEBFULLNAME 'Karl Kornel'

if (! $?prompt) exit    # exit if noninteractive -- starts faster

# Everything below this line is only for interactive shells

#-------------------------#
# Environmental Variables #
#-------------------------#

# Environmental variables are used by both shell and the programs
# the shell runs.

setenv EDITOR "vim"
if ( $?EDITOR ) then
    setenv VISUAL "$EDITOR"
endif

if ($?tcsh) then        # tcsh settings
    # alias precmd "source ~akkornel/bin/gitprompt.csh"
    unalias precmd
    set prompt="%m:%~%# " # set prompt to hostname:cwd>
    set autolist=ambiguous      # display possible completions
    set nobeep                  # do not beep if completions exist
    set listmax=50              # ask if completion has >50 matches
    set symlinks=ignore         # treat symlinks as real
    set pushdtohome             # pushd w/o arguments defaults to ~
    alias back 'cd -'           # chdir to previous directory

else                    # csh settings
    set filec                   # allow Esc filename completion
    set host=`hostname | sed -e 's/\..*//'`
    alias setprompt 'set prompt="${host}:$cwd:t%"'
    alias cd 'chdir \!* && setprompt'   # ensure prompt has cwd
    setprompt                   # set the prompt
endif

set history=100         # number of history commands to save
set notify              # immediate notification when jobs finish

# Git-related aliases
alias cdg "cd ~/git/\!*"
alias cdgi "cdg ikiwiki"
alias cdgp "cdg idg-rw"
alias cdgn "cdg nagios-config"
alias cdgnd "cdg nagios-config-dev"
alias ga 'git add'
alias gb 'git branch'
alias gbl 'git branch -l'
alias gbr 'git branch -r'
alias gc 'git commit'
alias gco 'git checkout'
alias gd 'git diff'
alias gp 'git pull --rebase'
alias gS 'git status'

# Scratch aliases
alias cds "cd $s/\!*"

alias lbdns "dig +noedns @lbdns1.stanford.edu \!^.best.stanford.edu txt"

# Command aliases
alias .. 'cd ..'                        # go up one directory
alias quiet '\!* >& /dev/null &'        # run a command with no output
alias orig 'cp -p \!:1 \!:1.orig'       # backup file

if ($?tcsh) then

# shell variable and alias commands
complete set 'p/1/s/=/'         # set <shell variable>=
complete printenv 'p/1/e/'      # printenv <Environmental variable>
complete unsetenv 'p/1/e/'      # unsetenv <Environmental variable>
complete setenv 'p/1/e/'        # setenv <Environmental variable>

endif

# turn off needless delays for special characters
stty cr0 nl0 -tabs ff0 bs0

# establish common bindings for terminal functions
stty intr '^C' erase '^?' kill '^U' quit '^\'

# set up common output settings
stty ixany -istrip
