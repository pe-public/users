if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

PATH=~/bin:$PATH
export PATH

umask 022
ulimit -c unlimited

alias cp='cp -i'
alias mv='mv -i'
alias cdg="cd ~/git"
alias cdgp="cdg; cd idg-rw"
alias ga="git add"
alias gb="git branch"
alias gbl="git branch -l"
alias gbr="git branch -r"
alias gc="git commit"
alias gco="git checkout"
alias gcob="git checkout -b"
alias gd="git diff"
alias gp="git pull --rebase"
alias gS="git status"
alias gt="git tag"
alias gta="git tag -a"
alias gtl="git tag -l"
alias gts="git tag -s"

# If not interactive shell, stop here to speed up processing
if [ -z "$PS1" ]; then
    return
fi

##############################################################################
# HISTORY CONFIGURATION
##############################################################################

# For some reason, the supposed default doesn't actually get set.
export HISTFILE="${HOME}/.bash_history"

# Set the limits of the history file
export HISTSIZE=4000
export HISTFILESIZE=4000

# Use ISO date format when displaying history.
export HISTTIMEFORMAT="%F %T: "

# Remove duplicates from the history file.
export HISTCONTROL=ignoredups

# Append to the history file rather than overwriting it.
shopt -s histappend

# Save and reload history after each command so that we share the history
# between all currently logged-in users.
export PROMPT_COMMAND="history -w; history -c; history -r"

##############################################################################
# VARIABLES, ALIASES, AND PROMPTING
##############################################################################

# By default, us vi as the editor
export EDITOR=vi
export VISUAL="$EDITOR"

# Notify when jobs complete asychroniously
set -b

# Prevent file clobbering during redirect
set -o noclobber

# Set the prompt
export PS1='\h:$(pwd)\$ '

# Don't require double tabs
bind "set show-all-if-ambiguous on"

# Automatically log out accounts after one day and one hour.
TMOUT=90000

# set facter library path to pickup our custom facter plugins
export FACTERLIB=/var/lib/puppet/lib/facter

##############################################################################
# TERMINAL SETTINGS
##############################################################################

# Disable needless delays for special characters.
stty cr0 nl0 tab0 ff0 bs0

# Make sure we have sane special character bindings.
stty intr '^C' erase '^?' kill '^U' quit '^\'

# Allow any character to restart output and do not strip input characters
# to seven bits.
stty ixany -istrip
