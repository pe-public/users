# !!!DO NOT EDIT OUTSIDE OF PUPPET!!!
# This is the Puppet-maintained .bashrc file for locally-created accounts.
# It is maintained in Puppet, so please change it there!
#
# The default file lives at this path:
# modules/user/files/root/bashrc
# To create a custom file just for you, put it here:
# modules/user/files/root/bashrc.custom/bashrc.USERNAME
#
# Make sure you commit your changes to stable, and then merge to master!

##############################################################################
# ALL SHELLS
##############################################################################

# Source the Red Hat global definitions if they exist
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# Aliases
alias cp='cp -i'
alias mv='mv -i'
alias afspath="export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/pubsw/bin:/usr/pubsw/sbin:/usr/sweet/bin:/usr/pubsw/X/bin"
alias noafspath="export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
alias oneshot='puppet-backend oneshot'

# Other settings
umask 022

# Set the dumpsize
ulimit -c unlimited

# If not interactive shell, stop here to speed up processing
if [ -z "$PS1" ]; then
    return
fi

##############################################################################
# HISTORY CONFIGURATION
##############################################################################

# For some reason, the supposed default doesn't actually get set.
export HISTFILE="${HOME}/.bash_history"

# Set the limits of the history file
export HISTSIZE=4000
export HISTFILESIZE=4000

# Use ISO date format when displaying history.
export HISTTIMEFORMAT="%F %T: "

# Remove duplicates from the history file.
export HISTCONTROL=ignoredups

# Append to the history file rather than overwriting it.
shopt -s histappend

# Save and reload history after each command so that we share the history
# between all currently logged-in users.
export PROMPT_COMMAND="history -w; history -c; history -r"

##############################################################################
# VARIABLES, ALIASES, AND PROMPTING
##############################################################################

# Source a user's custom configuration.  We have to use eval here since
# otherwise bash won't do tilde expansion.
b() {
    if [[ "$1" == "" ]]; then
        eval "source /root/.bashrc.extra/$USER"
    fi
    if [[ "$1" != "" ]]; then
        eval "source /root/.bashrc.extra/$1"
    fi
}

# By default, us vi as the editor
export EDITOR=vi
export VISUAL="$EDITOR"

# Notify when jobs complete asychroniously
set -b

# Prevent file clobbering during redirect
set -o noclobber

# Set the prompt
export PS1='\h:$(pwd)\$ '

# Don't require double tabs
bind "set show-all-if-ambiguous on"

# Automatically log out accounts after one day and one hour.
TMOUT=90000

# set facter library path to pickup our custom facter plugins
export FACTERLIB=/var/lib/puppet/lib/facter

##############################################################################
# TERMINAL SETTINGS
##############################################################################

# Disable needless delays for special characters.
stty cr0 nl0 tab0 ff0 bs0

# Make sure we have sane special character bindings.
stty intr '^C' erase '^?' kill '^U' quit '^\'

# Allow any character to restart output and do not strip input characters
# to seven bits.
stty ixany -istrip
