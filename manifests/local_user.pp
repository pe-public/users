define users::local_user (
  $ensure                   = 'present',
  $allowdupe                = false,
  $bash_profile_content     = undef,
  $bash_profile_source      = undef,
  $bashrc_content           = undef,
  $bashrc_source            = undef,
  $comment                  = $name,
  $create_group             = false,
  $expiry                   = undef,
  $forcelocal               = undef,
  $forward_content          = undef,
  $forward_source           = undef,
  $gid                      = undef,
  $group                    = undef,
  $groups                   = [ ],
  $gssapi                   = true,
  $home                     = undef,
  $home_mode                = undef,
  $ignore_password_if_empty = false,
  $locked                   = false,
  $managehome               = true,
  $managevim                = true,
  $membership               = 'minimum',
  $password                 = '!!',
  $password_max_age         = undef,
  $principals               = [],
  $purge_sshkeys            = false,
  $purge_user_home          = false,
  $shell                    = '/bin/bash',
  $sshkey_custom_path       = undef,
  $sshkey_group             = $group,
  $sshkey_owner             = $name,
  $sshkey_mode              = '0600',
  $sshkeys                  = [],
  $system                   = false,
  $uid                      = undef,
  $user                     = $name,
) {

  # Create user account passing all attributes along
  # to the Accounts module.
  accounts::user { $user:
    ensure                   => $ensure,
    allowdupe                => $allowdupe,
    bash_profile_content     => $bash_profile_content,
    bash_profile_source      => $bash_profile_source,
    bashrc_content           => $bashrc_content,
    bashrc_source            => $bashrc_source,
    comment                  => $comment,
    create_group             => $create_group,
    expiry                   => $expiry,
    forcelocal               => $forcelocal,
    forward_content          => $forward_content,
    forward_source           => $forward_source,
    gid                      => $gid,
    group                    => $group,
    groups                   => $groups,
    home                     => $home,
    home_mode                => $home_mode,
    ignore_password_if_empty => $ignore_password_if_empty,
    locked                   => $locked,
    managehome               => $managehome,
    managevim                => $managevim,
    membership               => $membership,
    password                 => $password,
    password_max_age         => $password_max_age,
    purge_sshkeys            => $purge_sshkeys,
    purge_user_home          => $purge_user_home,
    shell                    => $shell,
    sshkey_custom_path       => $sshkey_custom_path,
    sshkey_group             => $sshkey_group,
    sshkey_owner             => $sshkey_owner,
    sshkey_mode              => $sshkey_mode,
    sshkeys                  => $sshkeys,
    system                   => $system,
    uid                      => $uid,
  }

  if $managehome {
    $k5login_ensure = ($gssapi and ($ensure == 'present')) ? {
      true    => 'present',
      default => 'absent'
    }

    # create .k5login file if home dir is not in afs
    if !empty($principals) {
      k5login { "${home}/.k5login":
        ensure     => $k5login_ensure,
        principals => $principals,
        require    => Accounts::User[$user],
      }
    }
  }
}
