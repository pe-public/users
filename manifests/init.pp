# class to create users from hiera data # the users are divided into
#
# $sunet: hashes with user attributes who has  matching sunet ids
# $root:  attribute hashes for root users on the system
# $local: attribute hashes for local users on the system
#
# The attribute hashes are passed along to the Accounts module and support all the
# attributes which Accounts module supports. Local users hashes also support 'principals'
# attribute with list of principals allowed to access a local user account.
#
# $enable_duo:         Create root.sunetid accounts for root users
# $managed:            If true, all user accounts not defined in puppet will be deleted
# $local_homes:        False for home directories in afs.
# $gssapi:             If true, the module would create .k5login files in user accounts.
# $allow_root:         If not 'false', then direct logins to root account are allowed. 
#                      If 'gssapi', then a /root/.k5login file would be created. If set
#                      to 'pubkey', a collection of ssh keys would be put in
#                      /root/.ssh/authorized_keys. If password, none of these would be
#                      done (essentially equivalent to false).
#
# The module creates special groups whose members:
# admins               Allowed full access passwordless sudo
#
# $merge_with:         Used internally to optimize the number of hiera lookups

class users (
  $sunet = {},
  $root = {},
  $local = {},
  Boolean $local_homes = true,
  Boolean $managed = true,
  Boolean $enable_duo = true,
  Boolean $gssapi = true,
  Variant[Boolean[false], Enum['pubkey', 'gssapi', 'password']] $allow_root = false,
) {
  include users::params

  # remove non-system users not managed by puppet
  resources { 'user': purge => $managed }

  group { 'admins': ensure => 'present', gid => 10101 }

  # Lookup user attributes in hiera. Doing it here to minimize the 
  # number of hiera lookups.
  $user_data_src = $users::params::user_data_src
  $known_attrs = lookup($user_data_src, Hash, 'first', {})

  # If no defaults set, make them explicitly empty
  $defaults_sunet = ('defaults' in $sunet) ? { true => $sunet['defaults'], default => {} }

  # delete special pseudo accounts if any
  $sunet_sanitized = delete($sunet, ['root', 'defaults'])

  # Merge the attributes we've got from user hashes across hiera data sources
  # over the attributes in "defaults" account. Pass # the POSIX attributes from a 
  # user database in hiera to the # sunet_user resource unchanged. Process home
  # directories in a special way since in the "defaults" account we have only
  # home directory root, not a home diretory for a particular user.
  #
  $sunet_users = $sunet_sanitized.reduce({}) |$ms, $us| {

    # Home directories are a special attribute as a user id must be tacked on to the end.
    # If homes are not local, default home already exists in ldap, so we don't have to define it here.
    # For local homes check if there is a directory specified in "defaults" account and if
    # none, then use system default.
    #
    if $local_homes {
      $default_sunet_home = ('home' in $defaults_sunet) ? {
        true    => $defaults_sunet['home'],
        default => '/home'
      }
      $_defaults_sunet = $defaults_sunet + {
        home   => "${default_sunet_home}/${us[0]}",
        gssapi => $gssapi
      }
    } else {
      # Discard a home directory set in "defaults" account if it is supposed to be in afs.
      $_defaults_sunet = delete($defaults_sunet, ['home'])
    }

    $ms + { $us[0] => deep_merge(
      delete_undef_values({merge_with => $known_attrs[$us[0]]} + $_defaults_sunet), $us[1]) }
  }

  # create standard sunet user accounts
  create_resources('users::sunet_user', $sunet_users)

  # Direct logins to root account are enabled, create
  # .k5login file or authtorized_keys file depending on the 
  # desired method.
  #
  case $allow_root {
    'gssapi': {
      # Remove defaults account from consideration.
      $real_root = delete($root, ['defaults'])

      # If user hash is undefined, use its name to construct a root principal.
      # Otherwise use an array in principals key for a principal list.
      #
      $principals = flatten($real_root.map |$u| {
        ('principals' in $u[1]) ? {
          false   => "${u[0]}/root@stanford.edu",
          default => $u[1]['principals']
        }
      })

      k5login { '/root/.k5login':
        ensure     => 'present',
        principals => $principals,
        mode       => '600',
      }
    }

    'pubkey': {
      # Remove defaults account from consideration.
      $real_root = delete($root, ['defaults'])

      # Collect ssh keys from all root users
      $sshkeys = $real_root.reduce([]) |$mr, $u| {
        ('sshkeys' in $u[1]) ? {
          true    => $mr + $u[1]['sshkeys'],
          default => $mr
        }
      }

      # Create ssh key resources for root account using Accounts module.
      accounts::key_management { 'root':
        user      => 'root',
        group     => 'root',
        user_home => '/root',
        sshkeys   => $sshkeys,
      }
    }

    false: {
      # No direct logins to root account are allowed.
      # Clean up root account from authorization credentials
      file {[ '/root/.ssh/authorized_keys',
              '/root/.ssh/authorized_keys2',
              '/root/.k5login' ]:
        ensure => 'absent'
      }
    }
  }

  # create root accounts
  if $enable_duo {
    # If no defaults set, make them explicitly empty
    $defaults_root = ('defaults' in $root) ? { true => $root['defaults'], default => {} }

    # delete special pseudo accounts if any
    $root_sanitized = delete($root, ['root', 'defaults'])

    # Processing similar to sunet accounts with a difference that here we have to construct
    # a name for a root user account and give some sane defaults for a home directory if
    # none is provided in the "defaults" account.
    #
    $root_users = $root_sanitized.reduce({}) |$mr, $ur| {
      # Figure out the root user name from sunet id and a pattern
      $root_user_name = regsubst($ur[0], '^.*$', $users::params::root_name_pattern)

      $default_root_home = ('home' in $defaults_root) ? {
        true    => $defaults_root['home'],
        default => '/home'
      }
      $_defaults_root = $defaults_root + {
        home   => "${default_root_home}/${root_user_name}",
        gssapi => $gssapi
      }

      $mr + { $ur[0] => deep_merge(
        delete_undef_values({merge_with => $known_attrs[$ur[0]]} + $_defaults_root), $ur[1]) }
    }

    # create root user accounts
    create_resources('users::root_user', $root_users)
  }

  # Create accounts for the users with no SUNet IDs.
  # merge customized home directories over the default ones

  # if no defaults set, make them explicitly empty
  $defaults_local = ('defaults' in $local) ? { true => $local['defaults'], default => {} }

  # delete special pseudo accounts if any
  $local_sanitized = delete($local, ['root', 'defaults'])

  $local_users = $local_sanitized.reduce({}) |$ml, $ul| {
    # Home directories are a special attribute as a user id must be tacked on to the end.
    # If default home directory is not explicitly specified, whe have to provide one.
    $default_local_home = ('home' in $defaults_local) ? {
      true    => $defaults_local['home'],
      default => '/home'
    }

    $_defaults_local = $defaults_local + { home => "${default_local_home}/${ul[0]}" }
    $ml + { $ul[0] => deep_merge(delete_undef_values($_defaults_local), $ul[1]) }
  }

  # create user accounts
  create_resources('users::local_user', $local_users)

  # Create default shell startup files for the actual root user
  file { '/root/.bashrc':
    ensure       => 'present',
    source       => 'puppet:///modules/users/root/bashrc',
    sourceselect => 'first',
    owner        => 'root',
    mode         => '0600',
    replace      => false,
    force        => true,
  }

  file { '/root/.cshrc':
    ensure       => 'present',
    source       => 'puppet:///modules/users/root/cshrc',
    sourceselect => 'first',
    owner        => 'root',
    mode         => '0600',
    replace      => false,
    force        => true,
  }
}
