# $user_lookup_name: when looking up user information the `users` modules
#   will load data from the 'users::stanford_users' hash stored in the heira
#   data directory of `users`. However, if you want to store your user data
#   somewhere else override the `users::params::user_lookup_name`. This is
#   best explained with an example. If you store your user data in the
#   module `defaults::users`, set `users::params::user_lookup_name` to
#   `defaults::users`. See also the `tools` module documentation.

# $root_name_pattern: the account names for administrative users, who can
#   elevate their privileges with no password by default, are derived using
#   a regex pattern. The pattern must contain a backreference '\0', which gets
#   substituted with a sunet id of a user. For instance,
#
#   root.\0 pattern would generate users like root.atayts, root.adamhl, etc.
#   admin.\0  users like admin.atayts, admin.adamhl, etc.
#   \0.root users like atayts.root, adamhl.root, etc.

class users::params (
  $user_data_src  = 'users::stanford_users',
  $root_name_pattern = 'root.\0',
) {
  # sanity check for a root user name pattern
  if $root_name_pattern !~ /\\+0/ {
    fail('Root user name pattern must include a "\0" backreference.')
  }
}
