# defined resource for creating administrative users,
# who must login with their root kerberos principals.
#
# Parameter $merge_with is used internally for
# performance reasons. 

define users::root_user (
  $ensure                   = 'present',
  $allowdupe                = false,
  $bash_profile_content     = undef,
  $bash_profile_source      = undef,
  $bashrc_content           = undef,
  $bashrc_source            = undef,
  $comment                  = $name,
  $create_group             = false,
  $expiry                   = undef,
  $forcelocal               = undef,
  $forward_content          = undef,
  $forward_source           = undef,
  $gid                      = undef,
  $group                    = undef,
  $groups                   = undef,
  $gssapi                   = true,
  $home                     = undef,
  $home_mode                = undef,
  $ignore_password_if_empty = false,
  $locked                   = false,
  $managehome               = true,
  $managevim                = true,
  $membership               = 'minimum',
  $merge_with               = undef,
  $password                 = '!!',
  $password_max_age         = undef,
  $purge_sshkeys            = false,
  $purge_user_home          = false,
  $shell                    = undef,
  $sshkey_custom_path       = undef,
  $sshkey_group             = $group,
  $sshkey_owner             = undef,
  $sshkey_mode              = '0600',
  $sshkeys                  = [],
  $system                   = false,
  $uid                      = undef,
  $user                     = $name,
) {
  include users::params

  # If known user attributes taken from hiera data source are given
  # in a merge_with parameter, use them. Otherwise look up ourselves.
  # The latter would necessarily be the case when the resource is
  # instanticated directly from a manifest, not by "users" class.
  #
  if $merge_with {
    $known_attrs = $merge_with
  } else {
    $user_data_src = $users::params::user_data_src
    $known_attrs = lookup("${user_data_src}.${user}", Hash, 'first', {})
  }

  # fail immediately if user is not on the record
  if empty($known_attrs) {
      fail( "The user ${user} is unknown to puppet." )
  }

  # root users get uids in the range higher than
  # any sunet uid
  $uid_offset = 1000000
  $uid_root = Integer($known_attrs['uid']) + $uid_offset

  # Create root user name following a pattern
  $root_user_name = regsubst($user, "^.*$", $users::params::root_name_pattern)

  $attributes = deep_merge({
      ensure                   => 'present',
      uid                      => "${uid_root}",
      gid                      => '0',
      comment                  => $user,
      groups                   => [ 'admins' ],
      shell                    => $known_attrs['shell'],
      home                     => "/home/${root_user_name}",
      group                    => 'root',
      managehome               => true,
      create_group             => false,
      sshkey_owner             => $root_user_name
    }, delete_undef_values({
      ensure                   => $ensure,
      allowdupe                => $allowdupe,
      bash_profile_content     => $bash_profile_content,
      bash_profile_source      => $bash_profile_source,
      bashrc_content           => $bashrc_content,
      bashrc_source            => $bashrc_source,
      comment                  => $comment,
      create_group             => $create_group,
      expiry                   => $expiry,
      forcelocal               => $forcelocal,
      forward_content          => $forward_content,
      forward_source           => $forward_source,
      gid                      => $gid,
      group                    => $group,
      groups                   => ([ 'admins' ] + $groups).filter |$g| { $g =~ NotUndef },
      home                     => $home,
      home_mode                => $home_mode,
      ignore_password_if_empty => $ignore_password_if_empty,
      locked                   => $locked,
      managehome               => $managehome,
      managevim                => $managevim,
      membership               => $membership,
      password                 => $password,
      password_max_age         => $password_max_age,
      purge_sshkeys            => $purge_sshkeys,
      purge_user_home          => $purge_user_home,
      shell                    => $shell,
      sshkey_custom_path       => $sshkey_custom_path,
      sshkey_group             => $sshkey_group,
      sshkey_owner             => $sshkey_owner,
      sshkey_mode              => $sshkey_mode,
      sshkeys                  => $sshkeys,
      system                   => $system,
      uid                      => $uid,
    })
  )

  # create user account
  accounts::user { $root_user_name:
    *       => $attributes,
    require => Group['admins'],
  }

  if $managehome {
    # Generate .k5login files only if gssapi is enabled.
    $k5login_ensure = ($gssapi and ($attributes['ensure'] == 'present')) ? {
      true    => 'present',
      default => 'absent'
    }

    k5login { "${attributes['home']}/.k5login":
      ensure     => $k5login_ensure,
      principals => [ "${user}/root@stanford.edu" ],
      require    => Accounts::User[$root_user_name],
    }

    if $attributes['ensure'] == 'present' {
      # Create a .bashrc for the user.  Fall back to a default if needed.
      file { "${attributes['home']}/.bashrc":
        ensure       => $attributes['ensure'],
        source       => [
          "puppet:///modules/users/rootuser/bashrc.custom/bashrc.${name}",
          'puppet:///modules/users/rootuser/bashrc',
        ],
        sourceselect => 'first',
        owner        => $root_user_name,
        mode         => '0600',
        replace      => false,
        force        => true,
        require      => Accounts::User[$root_user_name]
      }

      # Create a .cshrc for the user.  Fall back to a default if needed.
      file { "${attributes['home']}/.cshrc":
        ensure       => $attributes['ensure'],
        source       => [
          "puppet:///modules/users/rootuser/cshrc.custom/cshrc.${name}",
          'puppet:///modules/users/rootuser/cshrc',
        ],
        sourceselect => 'first',
        owner        => $root_user_name,
        mode         => '0600',
        replace      => false,
        force        => true,
        require      => Accounts::User[$root_user_name]
      }
    }
  }
}
