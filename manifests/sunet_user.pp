# Defined resource for creating users with valid
# SUNet IDs. The attributes are passed along to
# the accounts module. Additional attribute "prinicpals"
# allows to add multiple principals to user's .k5login.
#
# Parameter $merge_with is used internally for
# performance reasons. 

define users::sunet_user (
  $ensure                   = undef,
  $allowdupe                = false,
  $bash_profile_content     = undef,
  $bash_profile_source      = undef,
  $bashrc_content           = undef,
  $bashrc_source            = undef,
  $comment                  = undef,
  $create_group             = false,
  $expiry                   = undef,
  $forcelocal               = undef,
  $forward_content          = undef,
  $forward_source           = undef,
  $gid                      = undef,
  $group                    = undef,
  $groups                   = [ ],
  $gssapi                   = true,
  $home                     = undef,
  $home_mode                = undef,
  $ignore_password_if_empty = false,
  $locked                   = false,
  $managehome               = undef,
  $managevim                = true,
  $membership               = 'minimum',
  $merge_with               = undef,
  $password                 = '!!',
  $password_max_age         = undef,
  $principals               = [],
  $purge_sshkeys            = false,
  $purge_user_home          = false,
  $shell                    = undef,
  $sshkey_custom_path       = undef,
  $sshkey_group             = $group,
  $sshkey_owner             = $name,
  $sshkey_mode              = '0600',
  $sshkeys                  = [],
  $system                   = false,
  $uid                      = undef,
  $user                     = $name,
) {

  include users::params

  # If known user attributes taken from hiera data source are given
  # in a merge_with parameter, use them. Otherwise look up ourselves.
  # The latter would necessarily be the case when the resource is
  # instanticated directly from a manifest, not by "users" class.
  # 
  if $merge_with {
    $known_attrs = $merge_with
  } else {
    $user_data_src = $users::params::user_data_src
    $known_attrs = lookup("${user_data_src}.${user}", Hash, 'first', {})
  }

  # fail immediately if user is not on the record
  if empty($known_attrs) {
      fail( "The user ${user} is unknown to puppet." )
  }

  $users_group = $::osfamily ? {
    'RedHat' => 'rpm',
    default  => 'operator'
  }

  # have to figure out if home is local before merging below
  # to set proper values for managehome and create_group
  $local_home = ($known_attrs['home'] !~ /^\/afs/) or ($home != undef)

  $attributes = deep_merge({
      ensure                   => 'present',
      uid                      => $known_attrs['uid'],
      gid                      => $known_attrs['gid'],
      comment                  => $known_attrs['comment'],
      shell                    => $known_attrs['shell'],
      home                     => $known_attrs['home'],
      group                    => $users_group,
      managehome               => $local_home,
      create_group             => $local_home
    }, delete_undef_values({
      ensure                   => $ensure,
      allowdupe                => $allowdupe,
      bash_profile_content     => $bash_profile_content,
      bash_profile_source      => $bash_profile_source,
      bashrc_content           => $bashrc_content,
      bashrc_source            => $bashrc_source,
      comment                  => $comment,
      create_group             => $create_group,
      expiry                   => $expiry,
      forcelocal               => $forcelocal,
      forward_content          => $forward_content,
      forward_source           => $forward_source,
      gid                      => $gid,
      group                    => $group,
      groups                   => $groups,
      home                     => $home,
      home_mode                => $home_mode,
      ignore_password_if_empty => $ignore_password_if_empty,
      locked                   => $locked,
      managehome               => $managehome,
      managevim                => $managevim,
      membership               => $membership,
      password                 => $password,
      password_max_age         => $password_max_age,
      purge_sshkeys            => $purge_sshkeys,
      purge_user_home          => $purge_user_home,
      shell                    => $shell,
      sshkey_custom_path       => $sshkey_custom_path,
      sshkey_group             => $sshkey_group,
      sshkey_owner             => $sshkey_owner,
      sshkey_mode              => $sshkey_mode,
      sshkeys                  => $sshkeys,
      system                   => $system,
      uid                      => $uid,
    })
  )

  # We use the PuppetForge module puppetlabs/accounts to create the
  # user account.
  accounts::user { $user:
    * => $attributes,
  }

  # now, for the users with local homes only
  if pick($managehome, $local_home) {
    # Generate .k5login files only if gssapi is enabled.
    $k5login_ensure = ($gssapi and ($attributes['ensure'] == 'present')) ? {
      true    => 'present',
      default => 'absent'
    }

    # create .k5login file
    k5login { "${attributes['home']}/.k5login":
      ensure     => $k5login_ensure,
      principals => empty($principals) ? {
        true    => "${user}@stanford.edu",
        default => $principals,
      },
      require    => Accounts::User[$user],
    }

    if $attributes['ensure'] == 'present' {

      # Create a .bashrc for the user.  Fall back to a default if needed.
      file { "${attributes['home']}/.bashrc":
        ensure       => $attributes['ensure'],
        source       => [
          "puppet:///modules/users/sunetuser/bashrc.custom/bashrc.${name}",
          'puppet:///modules/users/sunetuser/bashrc',
        ],
        sourceselect => 'first',
        owner        => $user,
        mode         => '0600',
        replace      => false,
        force        => true,
        require      => Accounts::User[$user]
      }

      # Create a .cshrc for the user.  Fall back to a default if needed.
      file { "${attributes['home']}/.cshrc":
        ensure       => $attributes['ensure'],
        source       => [
          "puppet:///modules/users/sunetuser/cshrc.custom/cshrc.${name}",
          'puppet:///modules/users/sunetuser/cshrc',
        ],
        sourceselect => 'first',
        owner        => $user,
        mode         => '0600',
        replace      => false,
        force        => true,
        require      => Accounts::User[$user]
      }
    }
  }
}
