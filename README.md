[[_TOC_]]

Users
=====

This is a wrapper around Puppetlabs' _Accounts_ module, which should technically
be a profile, but is not. Mostly to make the hiera data for this module shorter
and more intuitive.

## Module description
The module adds Stanford-specific features for user account handling:

* A data source with SUNet users, i.e. the users who have valid SUNet IDs.
The data source is handled by the scripts `tools/add_user.rb` and
`tools/del_user.rb`. Alternatively the data source can be edited manually. It
is a file `common.yaml` in module's own hiera.
* Creation of the server accounts for SUNet users either local to the server or in
AFS. Home directories can be specified for all users at once or individually
* Creation of the administrative accounts based on SUNet IDs, but with a string
prefix, `'root.'` by default.
* Creation of the local non-SUNet accounts.
* Handles `.k5login` files for kerberos authentication.

## Module parameters
Most of the parameters for each kind of Stanford user are identical to the
parameters of the _Accounts_ module. Together with `add_user.rb` script the module adds a
capability to create users with POSIX attributes matching Stanford LDAP. It also
adds management of .k5login files.

##### sunet
A hash of accounts::user resources to create server accounts for the users,
who have SUNet IDs.

##### root
A hash of accounts::user resources to create server accounts for administrative users.
The UIDs for these accounts are in a number range higher than SUNet IDs. The UIDs are
calculated by adding 1000000 to the UID of a user in Stanford LDAP.

When Duo authentication is disabled, the module treats administrative user accounts as a
list of kerberos root principals, which would be able to login to the server as `root`
account by adding these principlas to .k5login file for root user. Instead of inferring
a list of root principals from a user name, a list of principals can be given explicitly.

##### local
A hash of accounts::user resources to create local, non-SUNet ID server user accounts.

##### managed
If set to `true` will ensure that all user accounts not managed by puppet,
except system ones, will be removed. Make sure that it is set to `false` when user
accounts are pulled directly from LDAP.

##### local_homes
f set to `false`, SUNet user accounts would have AFS as their home directory.
Otherwise home directories inside local_home_dir are created.

##### enable_duo
Defines if Duo second factor authentication is enabled.

##### gssapi
If true, the module would create .k5login files for all generated accounts. Otherwise
it would remove existing .k5login files.

##### allow_root
Specifies if direct logins to the root account are desireable and what authentication
method should be used for them. Defaults to `false`, i.e no direct logins are required.
Can be set to `gssapi`, `pubkey` or `password`. If set to `gssapi`, a .k5login file in
the root accout account will be created. If set to `pubkey`, pubkeys from all administrative
users would be collected and placed into `authorized_keys` of the root account as well
as the additional keys one can provide to a special `root` administrative account.
`password` setting currently does nothing and has been added for compatibility with
`pam` and `ssh` modules.

## Classes and resources

### Class `users`

Collects user attributes defined in hiera, merges them with default account, and
instantates them as resources. Also manages .k5login an authorized_keys files for a
root user.

### Class `users::params`

For module-global parameters, use the parameters defined in `users::params`.

* `$user_data_src`: when looking up user information the `users`
modules will load data from the 'users::stanford_users' hash stored in the
hiera data directory of `users`. However, if you want to store your user
data somewhere else override the `users::params::user_data_src`. This
is best explained with an example. If you store your user data in the
module `defaults::users`, set `users::params::user_data_src` to
`defaults::users`. See also the documentation for the Puppet module
`tools` for more information on managing the user information.

* `$root_name_pattern`: regular expression allowing to define an arbitraty 
prefix and/or suffix to the user's sunet id to produce a name of an administrative user.
The name must contain a backreference `\0`, which gets substituted with a user's
sunet id. For instance, a pattern `\0.root` would produce account names `adamhl.root`,
`atayts.root`, etc. from sunet ids `adamhl` and `atayts` respectively.

### Resource `users::local_user`

Creates a local user with the attributes given in the parameters.

### Resource `users::sunet_user`

Creates a user with a valid SUNet account. The attributes given in the parameters are
merged with the attributes found in the user database `$user_data_src` in hiera. The user
database is managed with a set of scripts in _tools_ helper.

### Resource `users::root_user`

A bit misleadingly named, this class manages administrative users. These are standard users,
who typically have the right to elevate their privileges to root without entering a password.
These users are authenticated with their _root principals_ if GSSAPI is used as an
authentication method.


## Special groups

This module also creates a special group which is used to define
sudo privileges for administrative users. All users in `users::root` become members
of this group. It stands for "wheel" or "sudo" groups defined in default OS sudo
configurations, but is uniform across platforms and managed separately.

## Pseudo user accounts

There are a couple of special accounts, which can be defined in hiera and, as usual,
be overriden in downstream data sources. These account names are reserved.

##### "deafaults" account

This account specify defaults, which would be applied to all user accounts, unless
overriden on per-user basis or with an alternative defaults account in downstream hiera
data sources. Most obvious and useful application is to configure default home
directories. Other could be, for example, setting a common group which all users must
belong to or a default shell different from what is found in ldap.

##### "root" account

This special account works only in the context of administrative users, and only when DUO
second factor authentication is disabled. All attributes set for this account are ignored
with the exception of `principals` and `sshkeys`. The first attribute defines an array of 
principals, which are to be added to a `.k5login` file of the root account allowing these 
principals to login directly as *root*. Similarly `sshkeys` is an array of ssh pubkeys
which would be added to `/root/.ssh/authorized_keys` allowing the private key holders to
login directly as root.

The intended use of this account is to allow root logins to machine and service principals.
It is recommended not to add sunet-based principals to *root* account. The reason for this
is that if DUO gets enabled on the server, the user accounts added directly to the special
*root* account would lose access. The user accounts, which have been added to `users::root`
hash would not because the module would create local home directories for them and put the
corresponding `.k5login` and `authorized_keys` files in them.

## Examples

To use the module with hiera, just include it in your base profile with no parameters: `include users`. If you do not want to use hiera, you can instantiate the resources directly.

#### Minimal definition of each type of users

Define users in hiera:

```yaml
users::root:
  adamhl:
  atayts:
users::sunet:
  adamhl:
  atayts:
users::local:
  myuser:
    uid: '601'
    create_group: true
```

or the same in your manifest:

```yaml
users::root_user { ['adamhl', 'atayts']: }
users::sunet_user { ['adamhl', 'atayts']: }
users::local_user { 'myuser':
	uid => '601',
	create_group => true
}
```

#### More complicated

Local user which can login with a couple of ssh keys, actual password and a couple of
kerberos principals:

```yaml
users::local:
  localuser:
    uid: '1200'
    gid: '37'
    group: 'rpm'
    home: '/home/localuser'
    comment: 'Local User'
    shell: '/bin/bash'
    create_group: false
    principals:
      - 'atayts@stanford.edu'
      - 'allanh0@stanford.edu'
    password: '$6$9qB2T6AD$mLfyYTe.h0T96BJo874Birmc97XrNQlfK0SVxFij51hj.JWWQWwgB8s/I4MsKMalBiF6Ibq7F2O90AUBtHMOv.'
    sshkeys:
      - "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDYpNk\
         FNuwLfCfL0mwu2r6TCmWRoUpamxFyjcH/JKVULWxMYY\
         ...
         yUtkrJ5kbU3Z9CcehKzS73vjPM2tM2AD5Uhv atayts\
         @C02T101YGTJC"
      - "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA0vLI9Gp\
         QqDT84daT4SxKJ33151v86VRMCay4m3Tc89SNz/CcO1\
         ...
         t9/x94+OZkcYhwTy5He3LZKldhXcBpNrNw== atayts\
         @alexqs.stanford.edu"
```

SUNet user with one-off home directory, shell, additional group membership, custom home directory
permissions and allowing to login as a different user and a host principal.

```yaml
users::sunet:
  adem:
    groups:
      - 'webconfig'
    home: '/u/adem'
    shell: '/bin/ksh'
    home_mode: '0755'
    principals:
      - 'adem@stanford.edu'
      - 'atayts@stanford.edu'
      - 'host/server.stanford.edu'
```

Duo authentication is disabled and users _atayts_, _adamhl_ and _psr123_ are allowed to login
with their root principals. User _atayts_ can login with his regular principal as well.
Additionally allowed are principals _service/trusted_ and _host/server2.stanford.edu_.

#### Using *root* account

```yaml
users::enable_duo: false
users::root:
  atayts:
    principals:
      - 'atayts/root@stanford.edu'
      - 'atayts@stanford.edu'
  adamhl:
  psr123:
  root:
    principals:
      - 'service/trusted'
      - 'host/server2.stanford.edu'
```

The resulting contents of the `/root/.k5login` file would be:

```
atayts/root@stanford.edu
atayts@stanford.edu
adamhl/root@stanford.edu
psr123/root@stanford.edu
service/trusted
host/server2.stanford.edu
```

#### Using *defaults* account

```yaml
users::sunet:
  atayts:
  adamhl:
users::root:
  defaults:
    home: '/admins'
  atayts:
  adamhl:
    home: '/home'
  psr123:
```

In this, a bit contrived example, regular users would have their home directories in afs,
as they are specified in ldap. Administrative users in this example are given local
default home directories in `/admins` location and only `adamhl.root` would have a home
directory in `/home`. Of course, defaults can be specified for regular accounts as well.

