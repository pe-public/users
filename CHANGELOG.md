# Changelog

## [v2.2](https://code.stanford.edu/pe-public/users/-/tree/v2.2) (2021-08-30)

* Fix a bug in all user resources causing _managehome_ parameter be not
  respected.
* Add new parameters available in the later versions of the accounts module.

## [v2.1](https://code.stanford.edu/pe-public/users/-/tree/v2.1) (2021-02-07)

* Fix a bug in root_user resource which caused overwriting of the default
  admin group by additional groups, if they are specified.

## [v2.0](https://code.stanford.edu/pe-public/users/-/tree/v2.0) (2020-10-26)

* Create a special pseudo user account _defautls_, which can have the same
  attributes as a regular user account, but is is used for setting default
  attribute values for all users.
* Add `gssapi` parameter controlling creation of .k5login files.
* Add `allow_root` parameter controlling creation of .k5login files and
  keys in authorized_keys file for an actual root user.

### Breaking changes

* Remove `$local_home_dir` and `root_home_dir` parameters because they are
  obsoleted by the new "defaults" feature. 

## [v1.0](https://code.stanford.edu/pe-public/users/-/tree/v1.0) (2019-10-29)

* Initial release
